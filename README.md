Analyse de budgets publics.
==========================

Utilitaire d'analyse de budget publics.

Pour l'instant, focus est mis sur l'analyse de budget communaux belges.

Fonctionnalités
---------------

- Indique les grandes variations dans les codes économiques et fonctionnels entre plusieurs budgets consécutifs ;
- Indique les grandes variations dans des articles budgétaires entre plusieurs budgets consécutifs.


