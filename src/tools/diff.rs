use model::Article;
use std::collections::BTreeMap;
use std::collections::HashMap;

pub struct DiffAnalyzer {
    calcul: BTreeMap<String, HashMap<String, u32>>,
}

impl DiffAnalyzer {
    pub fn new() -> DiffAnalyzer {
        println!("Initialize DiffAnalyzer");

        DiffAnalyzer {
            calcul: BTreeMap::new(),
        }
    }

    pub fn push_article(&mut self, article: &Article) {
        let key: String = format!("{}.{}", article.econ, article.func);

        self.calcul
            .entry(key)
            .and_modify(|map| {
                let b: String = article.budget.clone();
                map.entry(b).and_modify(|s| *s += article.amount);
            })
            .or_insert({
                let b: String = article.budget.clone();
                let mut m: HashMap<String, u32> = HashMap::new();
                m.insert(b, article.amount);
                m
            });
    }
}

#[test]
fn simple_insert() {
    let mut a: DiffAnalyzer = DiffAnalyzer::new();
    let expected: u32 = 300_40;
    let art1: Article = Article {
        econ: String::from("123"),
        func: String::from("456"),
        amount: 100_20,
        budget: String::from("2018"),
        indice: String::from("3"),
    };
    let art2: Article = Article {
        amount: 200_20,
        indice: String::from("4"),
        econ: String::from("123"),
        func: String::from("456"),
        budget: String::from("2018"),
    };

    a.push_article(&art1);
    a.push_article(&art2);

    match a.calcul.get(&"123.456".to_string()) {
        Some(budgets) => {
            let sum: &u32 = budgets.get(&String::from("2018")).unwrap();
            assert_eq!(sum, &expected)
        }
        None => panic!("no entry found for 123.456"),
    };
}
