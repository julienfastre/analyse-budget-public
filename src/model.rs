pub struct Article {
    pub econ: String,
    pub func: String,
    pub indice: String,
    pub amount: u32,
    pub budget: String,
}
